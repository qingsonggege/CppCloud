package tool

import (
	"fmt"
	"testing"
)

func TestTypeConver(t *testing.T) {
	var ff float32 = 6.7
	var ss string = "1.23"

	var itff interface{}
	itff = ff
	s1 := fmt.Sprint(ff, ss)
	fmt.Println("ff:", s1)
	fmt.Println("ff->int:", int(ff))

	itff = ss
	fmt.Println("ss:", itff)
}

type Integer int

func TestTyper(t *testing.T) {
	var ii Integer = 1
	var itf interface{} = ii
	val, ok := itf.(int)

	fmt.Println(val, ok)

}

func TestHoMap(t *testing.T) {
	array := []interface{}{12, 34, "cpdcpd"}
	mp := HoMap{
		"hi":  "cppcloud",
		"ver": 1234,
		"arr": array,
	}

	valhi := JSONGetValue(mp, "hi")
	val := JSONGetValue(mp, "arr", 1)

	fmt.Println(valhi, val) // output: cppcloud 34
}

package main

import (
	"fmt"
	"protocol"
)

type Drived struct {
	*protocol.ProviderBase

	Mem string
}

func main() {
	obj := Drived{protocol.CreateProviderBase(nil), "dddd"}

	fmt.Println(obj.Mem)
	retStr := obj.Test1()
	fmt.Println("return value:", retStr)
}